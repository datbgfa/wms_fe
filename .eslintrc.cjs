/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
    root: true,
    extends: [
        'plugin:vue/vue3-essential',
        'eslint:recommended',
        '@vue/eslint-config-prettier/skip-formatting',
        // collections
        'vue-global-api/reactivity',
        'vue-global-api/lifecycle',
        'vue-global-api/component',
        // single apis
        'vue-global-api/ref',
        'vue-global-api/toRef',
        'prettier',
        'plugin:prettier/recommended',
        'plugin:vue/vue3-recommended',
    ],
    parserOptions: {
        ecmaVersion: 'latest'
    }
}
