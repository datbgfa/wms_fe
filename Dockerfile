FROM node:18-alpine3.17 AS build-stage
ENV TZ=Asia/Ho_Chi_Minh

WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM nginx:1.23.4-perl AS production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
COPY conf /etc/nginx/conf.d/default.conf
CMD ["nginx", "-g", "daemon off;"]