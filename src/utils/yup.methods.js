import * as yup from 'yup'

yup.addMethod(yup.string, 'isEmailOrUsername', function (errorMessage) {
    return this.test('isEmailOrUsername', errorMessage, function (value) {
        const { path, createError } = this

        if (isEmail(value) === true) {
            return true
        }

        if (isUser(value) === true) {
            return true
        }

        return createError({ path, message: errorMessage ?? 'Please input email or username!' })
    })
})

yup.addMethod(yup.string, 'isValidPassword', function (errorMessage) {
    return this.test('isValidPassword', errorMessage, function (value) {
        const { path, createError } = this

        if (isValidPassword(value) === true) {
            return true
        }

        return createError({
            path,
            message:
                errorMessage ??
                '"Password must be 8-32 characters, has number, uppercase letter and special character'
        })
    })
})

yup.addMethod(yup.string, 'isPhoneNumber', function (errorMessage) {
    return this.test('isPhoneNumber', errorMessage, function (value) {
        const { path, createError } = this

        if (isPhoneNumber(value) === true) {
            return true
        }

        return createError({
            path,
            message: errorMessage ?? 'Số điện thoại không hợp lệ'
        })
    })
})

const isEmail = (value) => {
    const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i
    return emailRegex.test(value)
}
const isPhoneNumber = (value) => {
    const phoneRegex = /(0|[3|5|7|8|9])+([0-9]{9})\b/g
    return phoneRegex.test(value)
}

const isUser = (value) => {
    const pattern = /^[A-Za-z0-9]+$/
    return pattern.test(value)
}

const isValidPassword = (value) => {
    const pattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,32}$/
    return pattern.test(value)
}

export default yup
