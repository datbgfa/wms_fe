import axios from 'axios'
import { useUserStore } from '@/stores/store.user'
import router from '@/router'

const instance = axios.create({
    baseURL: import.meta.env.VITE_APP_API_URL,
    timeout: 30000,
    headers: {
        'Content-Type': 'application/json'
    }
})

instance.interceptors.request.use(
    (config) => {
        const userStore = useUserStore()
        if (userStore.isAuthenticate() && config.url !== '/api/login') {
            config.headers = {
                Authorization: `Bearer ${userStore.state.accessToken}`
            }
        }

        return config
    },
    (error) => {
        Promise.reject(error)
    }
)

instance.interceptors.response.use(
    (response) => {
        return response.data
    },
    async (error) => {
        const originalRequest = error.config
        const userStore = useUserStore()

        if (
            error?.response?.config?.url === '/api/refresh-token' &&
            error?.response?.status === 401
        ) {
            userStore.removeAccesstoken()
            router.push('/')
            return Promise.reject(error)
        }

        if (error?.response?.config?.url === '/api/login' && error?.response?.status === 401) {
            return Promise.reject(error)
        }

        if (error?.response?.status === 401 && !originalRequest._retry) {
            originalRequest._retry = true

            const state = userStore.state
            const response = await userStore.refreshAccessToken()

            if (response) {
                originalRequest.headers = {
                    ...originalRequest.headers,
                    Authorization: `Bearer ${state.accessToken}`
                }
            } else {
                userStore.removeAccesstoken()
                return await router.push('/')
            }

            return instance(originalRequest)
        }

        if (error?.response?.status === 403 && error?.response?.config?.url !== '/api/login') {
            await router.push('/forbidden')
            return
        }

        return Promise.reject(error)
    }
)

export default instance
