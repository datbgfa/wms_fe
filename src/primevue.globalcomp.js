import {
    PrimeVue,
    ConfirmationService,
    ToastService,
    DialogService,
    Tooltip,
    BadgeDirective,
    Ripple,
    StyleClass,
    FocusTrap,
    Toolbar,
    Button,
    FileUpload,
    Column,
    InputText,
    Tag,
    DataTable,
    Dialog,
    Paginator,
    Skeleton,
    Textarea,
    Calendar,
    Dropdown,
    Image,
    Password,
    Toast,
    RadioButton,
    InputMask,
    InlineMessage,
    ToggleButton,
    InputSwitch,
    AutoComplete,
    Chips,
    MultiSelect,
    Listbox,
    Steps,
    InputNumber,
    TabMenu,
    TabView,
    TabPanel,
    Badge,
    Checkbox,
    Card,
    Chart
} from '@/primevue.components'

export const usePrimeVue = (app) => {
    app.use(PrimeVue, { ripple: true })
    app.use(ConfirmationService)
    app.use(ToastService)
    app.use(DialogService)

    app.directive('tooltip', Tooltip)
    app.directive('badge', BadgeDirective)
    app.directive('ripple', Ripple)
    app.directive('styleclass', StyleClass)
    app.directive('focustrap', FocusTrap)

    app.component('Column', Column)
    app.component('DataTable', DataTable)
    app.component('Dialog', Dialog)
    app.component('FileUpload', FileUpload)
    app.component('Tag', Tag)
    app.component('Toolbar', Toolbar)
    app.component('InputText', InputText)
    app.component('Button', Button)
    app.component('Paginator', Paginator)
    app.component('Skeleton', Skeleton)
    app.component('Password', Password)
    // app.component('Accordion', Accordion);
    // app.component('AccordionTab', AccordionTab);
    app.component('AutoComplete', AutoComplete)
    // app.component('Avatar', Avatar);
    // app.component('AvatarGroup', AvatarGroup);
    app.component('Badge', Badge)
    // app.component('BlockUI', BlockUI);
    // app.component('Breadcrumb', Breadcrumb);
    app.component('Calendar', Calendar)
    app.component('Card', Card)
    // app.component('Carousel', Carousel);
    // app.component('CascadeSelect', CascadeSelect);
    app.component('Checkbox', Checkbox)
    // app.component('Chip', Chip);
    app.component('Chips', Chips)
    // app.component('ColorPicker', ColorPicker);
    // app.component('ColumnGroup', ColumnGroup);
    // app.component('ConfirmDialog', ConfirmDialog);
    // app.component('ConfirmPopup', ConfirmPopup);
    // app.component('ContextMenu', ContextMenu);
    // app.component('DataView', DataView);
    // app.component('DataViewLayoutOptions', DataViewLayoutOptions);
    // app.component('DeferredContent', DeferredContent);
    // app.component('Divider', Divider);
    // app.component('Dock', Dock);
    app.component('Dropdown', Dropdown)
    // app.component('DynamicDialog', DynamicDialog);
    // app.component('Fieldset', Fieldset);
    // app.component('Galleria', Galleria);
    app.component('Image', Image)
    app.component('InlineMessage', InlineMessage)
    // app.component('Inplace', Inplace);
    app.component('InputMask', InputMask)
    app.component('InputSwitch', InputSwitch)
    app.component('InputNumber', InputNumber)
    // app.component('Knob', Knob);
    app.component('Listbox', Listbox)
    // app.component('MegaMenu', MegaMenu);
    // app.component('Menu', Menu);
    // app.component('Menubar', Menubar);
    // app.component('Message', Message);
    app.component('MultiSelect', MultiSelect)
    // app.component('OrderList', OrderList);
    // app.component('OrganizationChart', OrganizationChart);
    // app.component('OverlayPanel', OverlayPanel);
    // app.component('Panel', Panel);
    // app.component('PanelMenu', PanelMenu);
    // app.component('PickList', PickList);
    // app.component('ProgressBar', ProgressBar);
    // app.component('ProgressSpinner', ProgressSpinner);
    app.component('RadioButton', RadioButton)
    // app.component('Rating', Rating);
    // app.component('Row', Row);
    // app.component('SelectButton', SelectButton);
    // app.component('ScrollPanel', ScrollPanel);
    // app.component('ScrollTop', ScrollTop);
    // app.component('Slider', Slider);
    // app.component('Sidebar', Sidebar);
    // app.component('SpeedDial', SpeedDial);
    // app.component('SplitButton', SplitButton);
    // app.component('Splitter', Splitter);
    // app.component('SplitterPanel', SplitterPanel);
    app.component('Steps', Steps)
    app.component('TabMenu', TabMenu)
    app.component('TabView', TabView)
    app.component('TabPanel', TabPanel)
    app.component('Textarea', Textarea)
    // app.component('Terminal', Terminal);
    // app.component('TieredMenu', TieredMenu);
    // app.component('Timeline', Timeline);
    app.component('Toast', Toast)
    app.component('ToggleButton', ToggleButton)
    // app.component('Tree', Tree);
    // app.component('TreeSelect', TreeSelect);
    // app.component('TreeTable', TreeTable);
    // app.component('TriStateCheckbox', TriStateCheckbox);
    // app.component('VirtualScroller', VirtualScroller);
    app.component('Chart', Chart)
}
