import { defineStore } from 'pinia'
import axiosInstance from '@/utils/axios.instance'

export const useRequestImportStore = defineStore('requestimport', () => {
    const getBranchImportRequestList = async (URLPaginationParams) => {
        try {
            const response = await axiosInstance.get('/api/get-branch-import-request', {
                params: {
                    'PagingRequest.CurrentPage': URLPaginationParams.CurrentPage,
                    'PagingRequest.PageSize': URLPaginationParams.PageSize,
                    'PagingRequest.PageRange': URLPaginationParams.PageRange,
                    ApprovedSatus: URLPaginationParams.ApprovedSatus,
                    DateFilter: URLPaginationParams.DateFilter,
                    SearchWord: URLPaginationParams.SearchWord
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const getRequestImportDetails = async (requestImportId) => {
        try {
            const response = await axiosInstance.get('/api/get-branch-import-request-details', {
                params: {
                    branchRequestId: requestImportId
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const getBaseWarehouse = async () => {
        try {
            const response = await axiosInstance.get('/api/view-all-base-warehouses-list')
            return response.metaData[0]
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const approveRequestImport = async (requestId) => {
        try {
            const response = await axiosInstance.put(
                `/api/approve-branch-import-request/${requestId}`
            )
            return response
        } catch (error) {
            console.log(error)
        }
    }
    const getProductList = async (productName) => {
        try {
            const response = await axiosInstance.get('/api/get-product-by-name', {
                params: {
                    name: productName
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const updateRequestImport = async (updateDetails) => {
        try {
            const response = await axiosInstance.put(
                '/api/update-branch-import-request',
                updateDetails
            )
            return response
        } catch (error) {
            console.log(error)
        }
    }
    const createRequestImport = async (importDetails) => {
        try {
            const response = await axiosInstance.post(
                '/api/create-branch-import-request',
                importDetails
            )
            return response
        } catch (error) {
            console.log(error)
        }
    }
    const refuseRequestImport = async (requestId) => {
        try {
            const response = await axiosInstance.put(
                `/api/refuse-branch-import-request/${requestId}`
            )
            return response
        } catch (error) {
            console.log(error)
        }
    }
    return {
        getBranchImportRequestList,
        getRequestImportDetails,
        getBaseWarehouse,
        approveRequestImport,
        getProductList,
        updateRequestImport,
        createRequestImport,
        refuseRequestImport
    }
})
