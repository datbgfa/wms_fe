import { defineStore } from 'pinia'
import axiosInstance from '@/utils/axios.instance'

export const useBranchImportStore = defineStore('branchimport', () => {
    const getBranchImportList = async (URLPaginationParams) => {
        try {
            const response = await axiosInstance.get('/api/view-branch-import-list', {
                params: {
                    SearchWord: URLPaginationParams.SearchWord,
                    'PagingRequest.CurrentPage': URLPaginationParams.CurrentPage,
                    'PagingRequest.PageSize': URLPaginationParams.PageSize,
                    'PagingRequest.PageRange': URLPaginationParams.PageRange,
                    ImportStatus: URLPaginationParams.ImportStatus,
                    PaymentStatus: URLPaginationParams.PaymentStatus,
                    DateFilter: URLPaginationParams.DateFilter
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const getBranchImportDetails = async (branchImportId) => {
        try {
            const response = await axiosInstance.get('/api/view-branch-import-details', {
                params: {
                    branchImportId: branchImportId
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const changeStatusImport = async (reqInfo) => {
        try {
            const response = await axiosInstance.put(`/api/change-branch-import-status/${reqInfo}`)
            return response
        } catch (error) {
            console.log(error)
        }
    }
    const createProductReturnToBase = async (returnDetails) => {
        try {
            const response = await axiosInstance.post(
                '/api/create-product-return-to-base',
                returnDetails
            )
            return response
        } catch (error) {
            console.log(error)
        }
    }

    return {
        getBranchImportList,
        getBranchImportDetails,
        changeStatusImport,
        createProductReturnToBase
    }
})
