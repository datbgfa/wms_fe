import { defineStore } from 'pinia'
import axiosInstance from '@/utils/axios.instance'

export const useBaseExportStore = defineStore('baseexport', () => {
    const getBaseExportList = async (URLPaginationParams) => {
        try {
            const response = await axiosInstance.get('/api/view-exports-list', {
                params: {
                    SearchWord: URLPaginationParams.SearchWord,
                    'PagingRequest.CurrentPage': URLPaginationParams.CurrentPage,
                    'PagingRequest.PageSize': URLPaginationParams.PageSize,
                    'PagingRequest.PageRange': URLPaginationParams.PageRange,
                    ExportStatus: URLPaginationParams.ExportStatus,
                    PaymentStatus: URLPaginationParams.PaymentStatus,
                    DateFilter: URLPaginationParams.DateFilter,
                    BranchWarehouseId: URLPaginationParams.BranchWarehouseId
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return falseư
        }
    }
    const getBaseExportDetails = async (baseExportId) => {
        try {
            const response = await axiosInstance.get('/api/view-export-details', {
                params: {
                    exportId: baseExportId
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const updateBaseExport = async (updateDetails) => {
        try {
            const response = await axiosInstance.put('/api/update-base-export', updateDetails)
            return response
        } catch (error) {
            console.log(error)
        }
    }
    const getProductList = async (productName) => {
        try {
            const response = await axiosInstance.get('/api/get-product-by-name', {
                params: {
                    name: productName
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const payForBaseExport = async (reqInfo) => {
        try {
            const response = await axiosInstance.put('/api/pay-for-base-export', reqInfo)
            return response
        } catch (error) {
            console.log(error)
        }
    }
    const getProductInventory = async (productIds) => {
        try {
            const queryParams = productIds.map((id) => `productIds=${id}`).join('&')
            const response = await axiosInstance.get(`/api/get-product-quantity?${queryParams}`)
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const payForBranchProductReturn = async (reqInfo) => {
        try {
            const response = await axiosInstance.put('/api/pay-for-branch-product-return', reqInfo)
            return response
        } catch (error) {
            console.log(error)
        }
    }
    return {
        getBaseExportList,
        getBaseExportDetails,
        updateBaseExport,
        getProductList,
        payForBaseExport,
        getProductInventory,
        payForBranchProductReturn
    }
})
