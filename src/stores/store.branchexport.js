import { defineStore } from 'pinia'
import axiosInstance from '@/utils/axios.instance'

export const useBranchExportStore = defineStore('branchexport', () => {
    const getBranchExportList = async (URLPaginationParams) => {
        try {
            const response = await axiosInstance.get('/api/view-branch-exports-list', {
                params: {
                    SearchWord: URLPaginationParams.SearchWord,
                    'PagingRequest.CurrentPage': URLPaginationParams.CurrentPage,
                    'PagingRequest.PageSize': URLPaginationParams.PageSize,
                    'PagingRequest.PageRange': URLPaginationParams.PageRange,
                    DateFilter: URLPaginationParams.DateFilter
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return falseư
        }
    }
    const getBranchExportDetails = async (branchExportId) => {
        try {
            const response = await axiosInstance.get('/api/view-branch-export-details', {
                params: {
                    exportId: branchExportId
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const createBranchExport = async (exportDetails) => {
        try {
            const response = await axiosInstance.post('/api/create-branch-export', exportDetails)
            return response
        } catch (error) {
            console.log(error)
        }
    }
    const getProductList = async (productName) => {
        try {
            const response = await axiosInstance.get('/api/get-branch-product-in-stock-by-name', {
                params: {
                    searchWord: productName
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    return {
        getBranchExportList,
        getBranchExportDetails,
        createBranchExport,
        getProductList
    }
})
