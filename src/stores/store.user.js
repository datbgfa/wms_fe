import { defineStore } from 'pinia'
import { reactive } from 'vue'
import axiosInstance from '@/utils/axios.instance'
import jwt_decode from 'jwt-decode'
import router from '@/router'

const checkLocalStorage = (key) => {
    const data = localStorage.getItem(key)
    return data !== null && data !== undefined ? data.trim() : null
}

const ACCESS_TOKEN_KEY = 'accessToken'
const REFRESH_TOKEN_KEY = 'refreshToken'
const EMAIL_KEY = 'email'
const USERNAME_KEY = 'username'

export const useUserStore = defineStore('userStore', () => {
    const accessToken = checkLocalStorage(ACCESS_TOKEN_KEY)
    const refreshToken = checkLocalStorage(REFRESH_TOKEN_KEY)

    const state = reactive({
        refreshToken: refreshToken != null ? refreshToken : '',
        accessToken: accessToken != null ? accessToken : '',
        username: accessToken != null ? jwt_decode(accessToken).name : '',
        email: accessToken != null ? jwt_decode(accessToken).email : '',
        roles: []
    })

    const userLogin = async ({ username, password }) => {
        try {
            const response = await axiosInstance.post('/api/login', { username, password })
            if (response.code == 200) {
                const jwtDecoded = jwt_decode(response.metaData.accessToken)

                state.accessToken = response.metaData.accessToken
                state.refreshToken = response.metaData.refreshToken
                state.username = jwtDecoded.name
                state.email = jwtDecoded.email

                saveDataToLocalStorage(state)
            }

            return response?.code
        } catch (error) {
            return error?.response?.status
        }
    }

    const userSendEmail = async ({ username, email }) => {
        try {
            const response = await axiosInstance.put('/api/reset-password', { username, email })
            if (response.code == 200) {
                return true
            }
        } catch (error) {
            console.log(error)
            return false
        }
    }

    const logout = async () => {
        try {
            await axiosInstance.get('/api/logout')

            const isDeleted = removeDataInLocalStorage()
            if (isDeleted) {
                await router.push({ path: '/' })
            }
        } catch (error) {
            console.log(error)
        }
    }

    const saveDataToLocalStorage = (state) => {
        localStorage.setItem(REFRESH_TOKEN_KEY, state.refreshToken)
        localStorage.setItem(ACCESS_TOKEN_KEY, state.accessToken)
        localStorage.setItem(USERNAME_KEY, state.username)
        localStorage.setItem(EMAIL_KEY, state.email)
    }

    const removeDataInLocalStorage = () => {
        localStorage.removeItem(REFRESH_TOKEN_KEY)
        localStorage.removeItem(ACCESS_TOKEN_KEY)
        localStorage.removeItem(USERNAME_KEY)
        localStorage.removeItem(EMAIL_KEY)

        return true
    }

    const isAuthenticate = () => {
        return (
            checkLocalStorage(ACCESS_TOKEN_KEY) != null &&
            checkLocalStorage(REFRESH_TOKEN_KEY) != null
        )
    }

    const removeAccesstoken = () => {
        const accessToken = checkLocalStorage(ACCESS_TOKEN_KEY)

        if (accessToken !== null) {
            localStorage.removeItem(ACCESS_TOKEN_KEY)
        }
    }

    const profile = async () => {
        try {
            const response = await axiosInstance.get('/api/view-profile')
            return response.metaData
        } catch (error) {
            console.log(error)
        }
    }

    const canAccessScreen = async (screenId) => {}

    const getUserRole = async () => {
        try {
            const response = await axiosInstance.get('/api/get-role')
            return response.metaData
        } catch (error) {
            console.log(error)
        }
    }

    const editProfile = async (editProfile, avatarImage) => {
        try {
            const response = await axiosInstance.put('/api/edit-profile', editProfile)
            return response
        } catch (error) {
            console.log(error)
        }
    }

    const changePassword = async (requestValue) => {
        try {
            const response = await axiosInstance.put('/api/change-password', requestValue)
            if (response.code == 200) {
                return true
            }
        } catch (error) {
            console.log(error)
            return false
        }
    }

    const refreshAccessToken = async () => {
        try {
            const response = await axiosInstance.post('/api/refresh-token', {
                refreshToken: state.refreshToken
            })
            if (response?.code == 200) {
                const jwtDecoded = jwt_decode(response.metaData.accessToken)

                state.accessToken = response.metaData.accessToken
                state.refreshToken = response.metaData.refreshToken
                state.username = jwtDecoded.name
                state.email = jwtDecoded.email

                saveDataToLocalStorage(state)
            }

            return response
        } catch (error) {
            console.log(error)
            return null
        }
    }

    return {
        state,
        userLogin,
        isAuthenticate,
        userSendEmail,
        profile,
        editProfile,
        changePassword,
        refreshAccessToken,
        canAccessScreen,
        getUserRole,
        removeAccesstoken,
        logout
    }
})
