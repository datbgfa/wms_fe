import { defineStore } from 'pinia'
import axiosInstance from '@/utils/axios.instance'

export const useStockCheckSheetStore = defineStore('stockCheckSheet', () => {
    const getStockCheckSheetsList = async (requestValue) => {
        try {
            const response = await axiosInstance.get('/api/view-stock-check-sheet', {
                params: {
                    RoleId: requestValue.RoleId,
                    SearchWord: requestValue.SearchWord,
                    'PagingRequest.CurrentPage': requestValue.CurrentPage,
                    'PagingRequest.PageSize': requestValue.PageSize,
                    'PagingRequest.PageRange': requestValue.PageRange,
                    DateFilter: requestValue.DateFilter
                }
            })
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            console.log(error)
            return null
        }
    }

    const getStockCheckSheetById = async (requestValue) => {
        try {
            const response = await axiosInstance.get('/api/view-stock-check-sheet-by-id', {
                params: {
                    RoleId: requestValue.RoleId,
                    CheckId: requestValue.CheckId
                }
            })
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            console.log(error)
            return null
        }
    }

    const getStockCheckSheetDetails = async (requestValue) => {
        try {
            const response = await axiosInstance.get('/api/view-stock-check-sheet-details', {
                params: {
                    RoleId: requestValue.RoleId,
                    CheckId: requestValue.CheckId
                }
            })
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            console.log(error)
            return null
        }
    }

    const getStockCheckSheetImports = async (requestValue) => {
        try {
            const response = await axiosInstance.get('/api/view-stock-check-sheet-imports', {
                params: {
                    RoleId: requestValue.RoleId,
                    CheckId: requestValue.CheckId,
                    ProductId: requestValue.ProductId
                }
            })
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            console.log(error)
            return null
        }
    }

    const getImportsByProductId = async (requestValue) => {
        try {
            const response = await axiosInstance.get('/api/get-imports-by-productId', {
                params: {
                    RoleId: requestValue.RoleId,
                    ProductId: requestValue.ProductId
                }
            })
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            console.log(error)
            return null
        }
    }

    const createStockCheckSheet = async (requestValue) => {
        try {
            const response = await axiosInstance.post('/api/create-stock-check-sheet', requestValue)
            if (response.code == 200) {
                return true
            }
        } catch (error) {
            console.log(error)
            return false
        }
    }

    return {
        getStockCheckSheetsList,
        getStockCheckSheetDetails,
        getStockCheckSheetById,
        getStockCheckSheetImports,
        getImportsByProductId,
        createStockCheckSheet
    }
})
