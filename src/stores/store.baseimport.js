import { defineStore } from 'pinia'
import axiosInstance from '@/utils/axios.instance'

export const useBaseImportStore = defineStore('baseimport', () => {
    const getBaseImportList = async (URLPaginationParams) => {
        try {
            const response = await axiosInstance.get('/api/view-base-import-list', {
                params: {
                    SearchWord: URLPaginationParams.SearchWord,
                    'PagingRequest.CurrentPage': URLPaginationParams.CurrentPage,
                    'PagingRequest.PageSize': URLPaginationParams.PageSize,
                    'PagingRequest.PageRange': URLPaginationParams.PageRange,
                    ImportStatus: URLPaginationParams.ImportStatus,
                    PaymentStatus: URLPaginationParams.PaymentStatus,
                    DateFilter: URLPaginationParams.DateFilter,
                    SupplierId: URLPaginationParams.SupplierId
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const getSupplierList = async (URLPaginationParams) => {
        try {
            const response = await axiosInstance.get('/api/view-all-active-suppliers', {
                params: {
                    SearchWord: URLPaginationParams.SearchWord
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const getSupplierById = async (id) => {
        try {
            const response = await axiosInstance.get(`/api/get-supplier-details?supplierId=${id}`)
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            console.log(error)
            return null
        }
    }
    const getProductList = async (productName) => {
        try {
            const response = await axiosInstance.get('/api/get-product-by-name', {
                params: {
                    name: productName
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const getBaseImportDetails = async (baseImportId) => {
        try {
            const response = await axiosInstance.get('/api/view-base-import-details', {
                params: {
                    baseImportId: baseImportId
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const changeStatusImport = async (reqInfo) => {
        try {
            const response = await axiosInstance.put(`/api/change-base-import-status/${reqInfo}`)
            return response
        } catch (error) {
            console.log(error)
        }
    }
    const updateBaseImportNote = async (updateDetails) => {
        try {
            const response = await axiosInstance.put('/api/update-base-import-note', updateDetails)
            return response
        } catch (error) {
            console.log(error)
        }
    }
    const updateBaseImport = async (updateDetails) => {
        try {
            const response = await axiosInstance.put('/api/update-base-import', updateDetails)
            return response
        } catch (error) {
            console.log(error)
        }
    }
    const payForBaseImport = async (reqInfo) => {
        try {
            const response = await axiosInstance.put('/api/pay-for-base-import', reqInfo)
            return response
        } catch (error) {
            console.log(error)
        }
    }
    const payForBaseProductReturn = async (reqInfo) => {
        try {
            const response = await axiosInstance.put('/api/pay-for-base-product-return', reqInfo)
            return response
        } catch (error) {
            console.log(error)
        }
    }
    const createBaseImport = async (importDetails) => {
        try {
            const response = await axiosInstance.post('/api/create-base-import', importDetails)
            return response
        } catch (error) {
            console.log(error)
        }
    }
    const createProductReturnToSupplier = async (returnDetails) => {
        try {
            const response = await axiosInstance.post(
                '/api/create-product-return-to-supplier',
                returnDetails
            )
            return response
        } catch (error) {
            console.log(error)
        }
    }
    return {
        getBaseImportList,
        getSupplierList,
        getProductList,
        changeStatusImport,
        getBaseImportDetails,
        payForBaseImport,
        createBaseImport,
        updateBaseImportNote,
        getSupplierById,
        updateBaseImport,
        createProductReturnToSupplier,
        payForBaseProductReturn
    }
})
