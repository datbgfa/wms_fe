import { defineStore } from 'pinia'
import axiosInstance from '@/utils/axios.instance'

export const useEmployeeStore = defineStore('employee', () => {
    const getEmployeeList = async (URLPaginationParams) => {
        try {
            const response = await axiosInstance.get('/api/view-employees-list', {
                params: {
                    SearchWord: URLPaginationParams.SearchWord,
                    'PagingRequest.CurrentPage': URLPaginationParams.CurrentPage,
                    'PagingRequest.PageSize': URLPaginationParams.PageSize,
                    'PagingRequest.PageRange': URLPaginationParams.PageRange,
                    Status: URLPaginationParams.Status
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const getEmployeeDetails = async (employeeId) => {
        try {
            const response = await axiosInstance.get('/api/view-employees-details', {
                params: employeeId
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }

    const updateEmployeeDetail = async (updateInfo) => {
        try {
            const response = await axiosInstance.put('/api/update-employee-status', updateInfo)
            return response
        } catch (error) {
            console.log(error)
        }
    }

    const createEmployee = async (employeeInfo) => {
        try {
            const response = await axiosInstance.post('/api/create-employee', employeeInfo, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            return response
        } catch (error) {
            console.log(error)
        }
    }
    const getBaseEmployeeList = async () => {
        try {
            const response = await axiosInstance.get('/api/get-all-base-employee')
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const getBranchEmployeeList = async () => {
        try {
            const response = await axiosInstance.get('/api/get-all-branch-employee')
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }

    return {
        getEmployeeList,
        getEmployeeDetails,
        updateEmployeeDetail,
        createEmployee,
        getBaseEmployeeList,
        getBranchEmployeeList
    }
})
