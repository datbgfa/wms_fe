import { defineStore } from 'pinia'
import axiosInstance from '@/utils/axios.instance'
import { reactive } from 'vue'

export const useProductStore = defineStore('productStore', () => {
    const state = reactive({})

    const getProductList = async (URLPaginationParams) => {
        try {
            const response = await axiosInstance.get('/api/get-products', {
                params: {
                    SearchWord: URLPaginationParams.SearchWord,
                    'PagingRequest.CurrentPage': URLPaginationParams.CurrentPage,
                    'PagingRequest.PageSize': URLPaginationParams.PageSize,
                    'PagingRequest.PageRange': URLPaginationParams.PageRange,
                    Visible: URLPaginationParams.Visible,
                    CategoryId: URLPaginationParams.CategoryId
                }
            })

            if (response.code === 200) {
                return response.metaData
            }

            return {}
        } catch (error) {
            console.log(error)
        }
    }

    const getProductDetail = async (productId) => {
        try {
            const response = await axiosInstance.get(`/api/get-product-detail/${productId}`)

            if (response.code === 200) {
                return response.metaData
            }
            return response
        } catch (error) {
            console.log(error)
        }
    }

    const updateProducDetail = async (data) => {
        try {
            const { code } = await axiosInstance.put('/api/update-product', data, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })

            return code
        } catch (error) {
            console.log(error)
        }
    }

    const createProduct = async (newProduct) => {
        try {
            const response = await axiosInstance.post('/api/create-product', newProduct)

            return response
        } catch (error) {}
    }

    const getStockQuantity = async (requestValue) => {
        try {
            const response = await axiosInstance.get('/api/view-stock-quantity', {
                params: {
                    RoleId: requestValue.RoleId,
                    SearchWord: requestValue.SearchWord,
                    'PagingRequest.CurrentPage': requestValue.CurrentPage,
                    'PagingRequest.PageSize': requestValue.PageSize,
                    'PagingRequest.PageRange': requestValue.PageRange,
                    CategoryId: requestValue.CategoryId
                }
            })
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            return null
        }
    }

    const getStockQuantityById = async (requestValue) => {
        try {
            const response = await axiosInstance.get('/api/get-stock-quantity-by-productId', {
                params: {
                    RoleId: requestValue.RoleId,
                    ProductId: requestValue.ProductId
                }
            })
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            onsole.log(error)
            return null
        }
    }

    const getProductReturnFromBranch = async (requestValue) => {
        try {
            const response = await axiosInstance.get('/api/get-product-return-from-branch', {
                params: {
                    SearchWord: requestValue.SearchWord,
                    'PagingRequest.CurrentPage': requestValue.CurrentPage,
                    'PagingRequest.PageSize': requestValue.PageSize,
                    'PagingRequest.PageRange': requestValue.PageRange,
                    BranchWarehouseId: requestValue.BranchWarehouseId,
                    DateFilter: requestValue.DateFilter,
                    PaymentStatus: requestValue.PaymentStatus,
                    Status: requestValue.Status
                }
            })
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            console.log(error)
            return null
        }
    }

    const getProductListForBranch = async (URLPaginationParams) => {
        try {
            const response = await axiosInstance.get('/api/get-branch-warehouse-products', {
                params: {
                    SearchWord: URLPaginationParams.SearchWord,
                    'PagingRequest.CurrentPage': URLPaginationParams.CurrentPage,
                    'PagingRequest.PageSize': URLPaginationParams.PageSize,
                    'PagingRequest.PageRange': URLPaginationParams.PageRange,
                    CategoryId: URLPaginationParams.CategoryId
                }
            })

            if (response.code === 200) {
                return response.metaData
            }

            return {}
        } catch (error) {
            console.log(error)
        }
    }

    const updateProductForBranch = async (data) => {
        try {
            const { code } = await axiosInstance.put('/api/update-branch-warehouse-product', data)

            return code
        } catch (error) {
            console.log(error)
        }
    }

    const getProductDetailForBranch = async (productId) => {
        try {
            const response = await axiosInstance.get(
                `/api/get-branch-warehouse-product-details/${productId}`
            )

            if (response.code === 200) {
                return response.metaData
            }
            return response
        } catch (error) {
            console.log(error)
        }
    }

    const updateProductReturnStatusBranch = async (data) => {
        try {
            const response = await axiosInstance.put(
                '/api/update-product-return-status-from-branch',
                data
            )
            return response
        } catch (error) {
            console.log(error)
        }
    }

    return {
        state,
        getProductList,
        getProductDetail,
        updateProducDetail,
        createProduct,
        getStockQuantity,
        getStockQuantityById,
        getProductReturnFromBranch,
        getProductListForBranch,
        updateProductForBranch,
        getProductDetailForBranch,
        updateProductReturnStatusBranch
    }
})
