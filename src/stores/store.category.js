import { defineStore } from 'pinia'
import axiosInstance from '@/utils/axios.instance'

export const useCategoryStore = defineStore('category', () => {
    const getAllCategories = async (requestValue) => {
        try {
            const response = await axiosInstance.get('/api/view-categories-list', {
                params: {
                    SearchWord: requestValue.SearchWord,
                    'PagingRequest.CurrentPage': requestValue.CurrentPage,
                    'PagingRequest.PageSize': requestValue.PageSize,
                    'PagingRequest.PageRange': requestValue.PageRange,
                    Visible: requestValue.Visible
                }
            })
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            return null
        }
    }

    const addCategory = async (categoryName) => {
        try {
            const response = await axiosInstance.post('/api/create-category', categoryName)
            if (response.code == 200) {
                return true
            }
        } catch (error) {
            return false
        }
    }

    const updateCategory = async (category) => {
        try {
            const response = await axiosInstance.put('/api/update-category', category)
            if (response.code == 200) {
                return true
            }
        } catch (error) {
            return false
        }
    }

    const getCategories = async () => {
        try {
            const response = await axiosInstance.get('/api/view-all-categorys')
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            return null
        }
    }

    return { getAllCategories, addCategory, updateCategory, getCategories }
})
