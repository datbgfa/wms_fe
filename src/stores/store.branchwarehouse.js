import { defineStore } from 'pinia'
import axiosInstance from '@/utils/axios.instance'

export const useBranchWarehouseStore = defineStore('branchWarehouse', () => {
    const getBranchWarehouseList = async (URLPaginationParams) => {
        try {
            const response = await axiosInstance.get('/api/view-branch-warehouses-list', {
                params: {
                    SearchWord: URLPaginationParams.SearchWord,
                    'PagingRequest.CurrentPage': URLPaginationParams.CurrentPage,
                    'PagingRequest.PageSize': URLPaginationParams.PageSize,
                    'PagingRequest.PageRange': URLPaginationParams.PageRange,
                    Status: URLPaginationParams.Status
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const getAllBranchWarehouseList = async (hasOwner) => {
        try {
            const response = await axiosInstance.get('/api/view-all-branch-warehouses-list', {
                params: {
                    hasOwner: hasOwner
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const getBranchWarehouseDetails = async (warehouseId) => {
        try {
            const response = await axiosInstance.get(
                `/api/view-branch-warehouse-details?warehouseId=${warehouseId}`
            )
            // const response = await axiosInstance.get('/api/view-branch-warehouse-details', {
            //     params: warehouseId
            // })
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }

    const updateBranchWarehouseDetail = async (updateInfo) => {
        try {
            const response = await axiosInstance.put(
                '/api/update-branch-warehouse-details',
                updateInfo
            )
            return response
        } catch (error) {
            console.log(error)
        }
    }

    const createBranchWarehouse = async (newInfo) => {
        try {
            const response = await axiosInstance.post('/api/create-branch-warehouse', newInfo)
            return response
        } catch (error) {
            console.log(error)
        }
    }

    const getBranchDirectoresWithouWarehouse = async () => {
        try {
            const response = await axiosInstance.get('/api/get-branch-directors-without-warehouse')
            return response.metaData
        } catch (error) {
            console.log(error)
            return false
        }
    }

    return {
        getBranchWarehouseList,
        getBranchWarehouseDetails,
        updateBranchWarehouseDetail,
        createBranchWarehouse,
        getBranchDirectoresWithouWarehouse,
        getAllBranchWarehouseList
    }
})
