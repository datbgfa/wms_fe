import { defineStore } from 'pinia'
import axiosInstance from '@/utils/axios.instance'

export const useWarehouseStore = defineStore('warehouse', () => {
    const getProfileWarehouse = async () => {
        try {
            const response = await axiosInstance.get('/api/view-warehouse-profile')
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            console.log(error)
            return false
        }
    }

    const updateProfileWarehouse = async (warehouseInformation) => {
        try {
            const response = await axiosInstance.put(
                '/api/edit-warehouse-profile',
                warehouseInformation
            )
            return response
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const getAllBranchWarehouse = async () => {
        try {
            const response = await axiosInstance.get('/api/get-all-branch-warehouses')
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            return null
        }
    }

    const getBranchwarehouseByUsername = async () => {
        try {
            const response = await axiosInstance.get('/api/get-branch-warehouse-by-username')
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            console.log(error)
            return null
        }
    }
    const getWarehouseDashboard = async (requestTime) => {
        try {
            const response = await axiosInstance.get('/api/view-dashboard', {
                params: {
                    fromRq: requestTime.dateFrom,
                    toRq: requestTime.dateTo
                }
            })
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            console.log(error)
            return null
        }
    }

    const getBranchwarehouseByCurrentUser = async () => {
        try {
            const response = await axiosInstance.get('/api/get-branch-warehouse-by-current-user')
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            console.log(error)
            return null
        }
    }

    return {
        getProfileWarehouse,
        updateProfileWarehouse,
        getAllBranchWarehouse,
        getBranchwarehouseByUsername,
        getWarehouseDashboard,
        getBranchwarehouseByCurrentUser
    }
})
