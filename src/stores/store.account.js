import { defineStore } from 'pinia'
import axiosInstance from '@/utils/axios.instance'

export const useAccountStore = defineStore('account', () => {
    const getAllAccountList = async (URLPaginationParams) => {
        try {
            const response = await axiosInstance.get('/api/view-accounts-list', {
                params: {
                    SearchWord: URLPaginationParams.SearchWord,
                    'PagingRequest.CurrentPage': URLPaginationParams.CurrentPage,
                    'PagingRequest.PageSize': URLPaginationParams.PageSize,
                    'PagingRequest.PageRange': URLPaginationParams.PageRange,
                    Status: URLPaginationParams.Status
                }
            })
            return response.metaData
        } catch (error) {
            console.log(error)
        }
    }

    const changeStatusAccount = async (reqInfo) => {
        try {
            const response = await axiosInstance.put('/api/change-account-status', reqInfo)
            return response
        } catch (error) {
            console.log(error)
        }
    }
    const createAccountEmployee = async (accountInfo) => {
        try {
            const response = await axiosInstance.post('/api/create-employee-account', accountInfo, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            return response
        } catch (error) {
            console.log(error)
        }
    }
    const getBaseWarehouse = async () => {
        try {
            const response = await axiosInstance.get('/api/view-all-base-warehouses-list')
            return response.metaData[0]
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const checkEmailExist = async (email) => {
        try {
            const response = await axiosInstance.get('/api/check-email-exist', {
                params: {
                    email: email
                }
            })
            return response
        } catch (error) {
            console.log(error)
            return false
        }
    }
    const checkUsernameExist = async (username) => {
        try {
            const response = await axiosInstance.get('/api/check-username-exist', {
                params: {
                    username: username
                }
            })
            return response
        } catch (error) {
            console.log(error)
            return false
        }
    }

    return {
        getAllAccountList,
        changeStatusAccount,
        createAccountEmployee,
        getBaseWarehouse,
        checkEmailExist,
        checkUsernameExist
    }
})
