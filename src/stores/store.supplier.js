import { defineStore } from 'pinia'
import axiosInstance from '@/utils/axios.instance'

export const useSupplierStore = defineStore('supplier', () => {
    const getAllSuppliers = async (requestValue) => {
        try {
            const response = await axiosInstance.get('/api/view-suppliers-list', {
                params: {
                    SearchWord: requestValue.SearchWord,
                    'PagingRequest.CurrentPage': requestValue.CurrentPage,
                    'PagingRequest.PageSize': requestValue.PageSize,
                    'PagingRequest.PageRange': requestValue.PageRange
                }
            })
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            console.log(error)
            return null
        }
    }

    const getSupplierById = async (id) => {
        try {
            const response = await axiosInstance.get(`/api/get-supplier-details?supplierId=${id}`)
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            console.log(error)
            return null
        }
    }

    const addSupplier = async (requestValue) => {
        try {
            const response = await axiosInstance.post('/api/create-supplier', requestValue, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            if (response.code == 200) {
                return true
            }
        } catch (error) {
            console.log('Error')
            return false
        }
    }

    const updateSupplier = async (requestValue) => {
        try {
            const response = await axiosInstance.put('/api/update-supplier', requestValue, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            if (response.code == 200) {
                return true
            }
        } catch (error) {
            console.log('Error')
            return false
        }
    }

    const updateSupplierStatus = async (requestValue) => {
        try {
            const response = await axiosInstance.put('/api/update-supplier-status', requestValue)
            if (response.code == 200) {
                return true
            }
        } catch (error) {
            console.log('Error')
            return false
        }
    }

    const getHistoryImport = async (id) => {
        try {
            const response = await axiosInstance.get(`/api/get-history-import?supplierId=${id}`)
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            console.log(error)
            return null
        }
    }

    const getDebts = async (id) => {
        try {
            const response = await axiosInstance.get(`/api/get-debts?supplierId=${id}`)
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            console.log(error)
            return null
        }
    }

    const getAllSupplier = async () => {
        try {
            const response = await axiosInstance.get(`/api/view-all-suppliers-list`)
            if (response.code == 200) {
                return response.metaData
            }
        } catch (error) {
            console.log(error)
            return null
        }
    }

    return {
        getAllSuppliers,
        getSupplierById,
        addSupplier,
        updateSupplier,
        updateSupplierStatus,
        getHistoryImport,
        getDebts,
        getAllSupplier
    }
})
