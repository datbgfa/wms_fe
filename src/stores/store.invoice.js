import { defineStore } from 'pinia'
import axiosInstance from '@/utils/axios.instance'
import lodash from 'lodash'

export const useInvoiceStore = defineStore('invoice', () => {
    const getBaseInvoice = async (request) => {
        try {
            let requestParams = {}

            if (!lodash.isEmpty(request.from)) {
                requestParams.From = request.from
            }

            if (!lodash.isEmpty(request.to)) {
                requestParams.To = request.to
            }

            requestParams.Type = request.type
            requestParams.InvoiceFor = request.invoiceFor
            requestParams.CurrentPage = request.CurrentPage
            requestParams.PageSize = request.PageSize
            requestParams.PageRange = request.PageRange

            const response = await axiosInstance.get('/api/get-base-invoices', {
                params: requestParams
            })

            if (response.code === 200) {
                return response.metaData
            }

            return {}
        } catch (error) {
            console.log(error)
        }
    }

    const getBranchInvoice = async (request) => {
        try {
            let requestParams = {}

            if (!lodash.isEmpty(request.from)) {
                requestParams.From = request.from
            }

            if (!lodash.isEmpty(request.to)) {
                requestParams.To = request.to
            }

            requestParams.Type = request.type
            requestParams.InvoiceFor = request.invoiceFor
            requestParams.CurrentPage = request.CurrentPage
            requestParams.PageSize = request.PageSize
            requestParams.PageRange = request.PageRange

            const response = await axiosInstance.get('/api/get-branch-invoices', {
                params: requestParams
            })

            if (response.code === 200) {
                return response.metaData
            }

            return {}
        } catch (error) {
            console.log(error)
        }
    }

    const createPaymentInvoice = async (request) => {
        try {
            const response = await axiosInstance.post('/api/create-payment-invoice', request)

            return response.code === 200
        } catch (error) {
            console.log(error)
        }
    }

    const createReceiptInvoice = async (request) => {
        try {
            const response = await axiosInstance.post('/api/create-receipt-invoice', request)

            return response.code === 200
        } catch (error) {
            console.log(error)
        }
    }

    const getSupplierWithDebt = async (request) => {
        try {
            const response = await axiosInstance.get('/api/getall-have-debts')

            if (response.code === 200) {
                return response.metaData
            }

            return []
        } catch (error) {
            console.log(error)
        }
    }

    const getBranchWithDebt = async (request) => {
        try {
            const response = await axiosInstance.get('/api/get-branch-have-debt')

            if (response.code === 200) {
                return response.metaData
            }

            return []
        } catch (error) {
            console.log(error)
        }
    }

    return {
        getBaseInvoice,
        createPaymentInvoice,
        createReceiptInvoice,
        getSupplierWithDebt,
        getBranchWithDebt,
        getBranchInvoice
    }
})
