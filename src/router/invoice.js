const invoice = [
    {
        path: '/base-invoice',
        name: 'base-invoice',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM']
        },
        component: () => import('@/views/ReceiptPaymentInvoiceViews/BaseInvoiceView.vue')
    },
    {
        path: '/branch-invoice',
        name: 'branch-invoice',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Branch_WD']
        },
        component: () => import('@/views/ReceiptPaymentInvoiceViews/BranchInvoiceView.vue')
    }
]

export default invoice
