const publics = [
    {
        path: '/base-import-list',
        name: 'base-import-list',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM', 'Base_WE']
        },
        component: () => import('@/views/BaseImportManagementViews/ImportListView.vue')
    },
    {
        path: '/create-base-import',
        name: 'create-base-import',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM', 'Base_WE']
        },
        component: () => import('@/views/BaseImportManagementViews/CreateImport.vue')
    },
    {
        path: '/base-import-detail/:importId',
        name: 'base-import-detail',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM', 'Base_WE']
        },
        component: () => import('@/views/BaseImportManagementViews/ViewImportDetail.vue')
    },
    {
        path: '/update-base-import/:importId',
        name: 'update-base-import',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM', 'Base_WE']
        },
        component: () => import('@/views/BaseImportManagementViews/UpdateBaseImport.vue')
    },
    {
        path: '/create-base-product-return/:importId',
        name: 'create-base-product-return',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM', 'Base_WE']
        },
        component: () =>
            import('@/views/BaseImportManagementViews/CreateBaseProductReturnToSupplier.vue')
    }
]

export default publics
