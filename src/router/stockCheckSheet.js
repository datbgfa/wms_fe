const publics = [
    {
        path: '/stock-check-sheet-management',
        name: 'stock-check-sheet-management',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM', 'Base_WE', 'Branch_WD', 'Branch_WE', 'Branch_WM']
        },
        component: () =>
            import('@/views/StockCheckSheetManagementViews/StockCheckSheetManagementView.vue')
    },
    {
        path: '/stock-check-sheet-details/:checkId',
        name: 'stock-check-sheet-details',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM', 'Base_WE', 'Branch_WD', 'Branch_WE', 'Branch_WM']
        },
        component: () =>
            import('@/views/StockCheckSheetManagementViews/StockCheckSheetDetailView.vue'),
        props: true
    },
    {
        path: '/create-stock-check-sheet',
        name: 'create-stock-check-sheet',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM', 'Base_WE', 'Branch_WD', 'Branch_WE', 'Branch_WM']
        },
        component: () => import('@/views/StockCheckSheetManagementViews/AddStockCheckSheetView.vue')
    }
]

export default publics
