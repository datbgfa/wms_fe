import { createRouter, createWebHistory } from 'vue-router'
import publics from './public/route.public'
import warehouse from '@/router/warehouseRoute.js'
import product from '@/router/product'
import baseImport from '@/router/baseImport.js'
import baseExport from '@/router/baseExport.js'
import branchImport from '@/router/branchImport.js'
import branchExport from '@/router/branchExport.js'
import importRequest from '@/router/importRequest.js'
import stockCheckSheet from '@/router/stockCheckSheet'
import invoice from '@/router/invoice'
import error from '@/router/error.js'
import { useUserStore } from '@/stores/store.user'

const routes = [
    ...publics,
    ...warehouse,
    ...product,
    ...baseImport,
    ...importRequest,
    ...baseExport,
    ...branchImport,
    ...branchExport,
    ...stockCheckSheet,
    ...error,
    ...invoice
]

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: routes
})

router.beforeEach(async (to, from, next) => {
    const userStore = useUserStore()
    if (to.matched.some((rec) => rec.meta.isAuth) && !userStore.isAuthenticate()) {
        next({ path: '/' })
    } else if (userStore.isAuthenticate()) {
        switch (to.path) {
            case '/':
                next({ path: '/home' })
                break

            case '/forgot-password':
                next({ path: '/home' })
                break

            default:
                next()
                break
        }
    } else {
        next()
    }
})

export default router
