const publics = [
    {
        path: '/branch-import-list',
        name: 'branch-import-list',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Branch_WD', 'Branch_WE', 'Branch_WM']
        },
        component: () => import('@/views/BranchImportManagementViews/ViewBranchImportList.vue')
    },
    {
        path: '/branch-import-detail/:importId',
        name: 'branch-import-detail',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Branch_WD', 'Branch_WE', 'Branch_WM']
        },
        component: () => import('@/views/BranchImportManagementViews/BranchImportDetail.vue')
    },
    {
        path: '/create-branch-product-return/:importId',
        name: 'create-branch-product-return',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Branch_WD', 'Branch_WE', 'Branch_WM']
        },
        component: () =>
            import('@/views/BranchImportManagementViews/CreateBranchProductReturnToBase.vue')
    }
]

export default publics
