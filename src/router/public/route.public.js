import HomeView from '@/views/HomeView.vue'

const publics = [
    {
        path: '/',
        name: 'login',
        meta: {
            layout: 'default',
            isAuth: false
        },
        component: () => import('@/views/PublicViews/LoginView.vue')
    },
    {
        path: '/login',
        redirect: '/'
    },

    {
        path: '/forgot-password',
        name: 'forgot-password',
        meta: {
            layout: 'default',
            isAuth: false
        },
        component: () => import('@/views/PublicViews/ResetPasswordView.vue')
    },
    {
        path: '/home',
        name: 'home',
        meta: {
            layout: 'auth',
            isAuth: true
        },
        component: HomeView
    },
    {
        path: '/account-management',
        name: 'account-management',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Admin']
        },
        component: () => import('@/views/ManageAccountViews/AccountListView.vue')
    },
    {
        path: '/testlayout',
        name: 'testlayout',
        meta: {
            layout: 'auth',
            isAuth: true
        }
    },
    {
        path: '/profile',
        name: 'profile',
        meta: {
            layout: 'auth',
            isAuth: true
        },
        component: () => import('@/views/ProfilePage/index.vue')
    },
    {
        path: '/category-management',
        name: 'category-management',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM']
        },
        component: () => import('@/views/CategoryManagementViews/CategoryManagementView.vue')
    },
    {
        path: '/employee-management',
        name: 'employee-management',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM', 'Branch_WD']
        },
        component: () => import('@/views/EmployeeManagementViews/EmployeeManagementView.vue')
    },
    {
        path: '/employee-details',
        name: 'employee-details',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM', 'Branch_WD']
        },
        component: () => import('@/views/EmployeeManagementViews/EmployeeDetailView.vue')
    },
    {
        path: '/supplier-management',
        name: 'supplier-management',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM']
        },
        component: () => import('@/views/SupplierManagementViews/SupplierManagementView.vue')
    },
    {
        path: '/supplier-details/:supplierId',
        name: 'supplier-details',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM']
        },
        component: () => import('@/views/SupplierManagementViews/SupplierDetailsView.vue'),
        props: true
    },
    {
        path: '/stock-quantity-management',
        name: 'stock-quantity-management',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM', 'Base_WE', 'Branch_WD', 'Branch_WE', 'Branch_WM']
        },
        component: () =>
            import('@/views/StockQuantityManagementViews/StockQuantityManagementView.vue'),
        props: true
    }
    // ,
    // {
    //     path: '/my-test',
    //     name: 'my-test',
    //     meta: {
    //         layout: 'auth',
    //         isAuth: true
    //     },
    //     component: () => import('@/views/')
    // }
]

export default publics
