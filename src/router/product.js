const publics = [
    {
        path: '/create-product',
        name: 'create-product',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM']
        },
        component: () => import('@/views/ProductPage/CreateProductView.vue')
    },
    {
        path: '/base/product-return-management',
        name: 'base/product-return-management',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM']
        },
        component: () =>
            import(
                '@/views/ProductReturnRequestFromBranchWarehouseViews/ProductReturnRequestFromBranchWarehouseView.vue'
            )
    },
    {
        path: '/product-management',
        name: 'product-management',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM', 'Base_WE', 'Branch_WD', 'Branch_WE', 'Branch_WM']
        },
        component: () => import('@/views/ProductPage/ProductListView.vue')
    },
    {
        path: '/branch-product-management',
        name: 'branch-product-management',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Branch_WD', 'Branch_WE', 'Branch_WM']
        },
        component: () => import('@/views/ProductBranchPage/ProductListForBranchView.vue')
    }
]

export default publics
