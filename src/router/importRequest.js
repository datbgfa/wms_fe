const publics = [
    {
        path: '/branch-import-request',
        name: 'branch-import-request',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Branch_WD', 'Branch_WE', 'Branch_WM']
        },
        component: () =>
            import('@/views/BranchImportRequestManagementViews/ImportRequestListView.vue')
    },
    {
        path: '/branch-import-request-detail/:requestId',
        name: 'branch-import-request-detail',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Branch_WD', 'Branch_WE', 'Branch_WM']
        },
        component: () =>
            import('@/views/BranchImportRequestManagementViews/ImportRequestDetail.vue')
    },
    {
        path: '/update-branch-import-request/:requestId',
        name: 'update-branch-import-request',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Branch_WD', 'Branch_WM']
        },
        component: () =>
            import('@/views/BranchImportRequestManagementViews/UpdateImportRequest.vue')
    },
    {
        path: '/create-branch-import-request',
        name: 'create-branch-import-request',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Branch_WD', 'Branch_WM']
        },
        component: () =>
            import('@/views/BranchImportRequestManagementViews/CreateImportRequest.vue')
    }
]

export default publics
