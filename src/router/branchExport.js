const publics = [
    {
        path: '/branch-export-list',
        name: 'branch-export-list',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Branch_WD', 'Branch_WE', 'Branch_WM']
        },
        component: () => import('@/views/BranchExportManagementViews/ViewBranchExportList.vue')
    },
    {
        path: '/branch-export-detail/:exportId',
        name: 'branch-export-detail',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Branch_WD', 'Branch_WE', 'Branch_WM']
        },
        component: () => import('@/views/BranchExportManagementViews/ViewBranchExportDetail.vue')
    },
    {
        path: '/create-branch-export',
        name: 'create-branch-export',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Branch_WD', 'Branch_WM']
        },
        component: () => import('@/views/BranchExportManagementViews/CreateBranchExport.vue')
    }
]

export default publics
