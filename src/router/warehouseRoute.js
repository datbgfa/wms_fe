const publics = [
    {
        path: '/view-warehouse-profile',
        name: 'view-warehouse-profile',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM', 'Branch_WD']
        },
        component: () => import('@/views/ProfileWarehouseViews/ProfileWarehouse.vue')
    },
    {
        path: '/view-branch-warehouses',
        name: 'view-branch-warehouses',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM']
        },
        component: () => import('@/views/BranchWarehouseViews/BranchWarehouseViews.vue')
    },
    {
        path: '/branch-warehouse-details/:branchWarehouseId',
        name: 'branch-warehouse-details',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM']
        },
        component: () => import('@/views/BranchWarehouseViews/BranchWarehouseDetailsView.vue')
    }
]

export default publics
