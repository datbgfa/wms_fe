const publics = [
    {
        path: '/base-export-list',
        name: 'base-export-list',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM', 'Base_WE']
        },
        component: () => import('@/views/BaseExportManagementViews/BaseExportListView.vue')
    },
    {
        path: '/base-export-detail/:exportId',
        name: 'base-export-detail',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM', 'Base_WE']
        },
        component: () => import('@/views/BaseExportManagementViews/ViewBaseExportDetail.vue')
    },
    {
        path: '/update-base-export/:exportId',
        name: 'update-base-export',
        meta: {
            layout: 'auth',
            isAuth: true,
            canAccess: ['Base_WM', 'Base_WE']
        },
        component: () => import('@/views/BaseExportManagementViews/UpdateBaseExport.vue')
    }
]

export default publics
