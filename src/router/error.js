const publics = [
    {
        path: '/forbidden',
        name: '403-forbidden',
        meta: {
            layout: 'default'
        },
        component: () => import('@/views/ErrorPage/403Page.vue')
    },
    {
        path: '/not-found',
        name: '404-pagenotfound',
        meta: {
            layout: 'default'
        },
        component: () => import('@/views/ErrorPage/404Page.vue')
    },
    {
        path: '/:pathMatch(.*)*',
        redirect: '/not-found'
    }
]

export default publics
