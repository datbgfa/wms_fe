export const roles = {
    Admin: {
        roleId: 1,
        roleName: 'Admin',
        roleDescription: 'Administrator'
    },
    Base_WM: {
        roleId: 2,
        roleName: 'Base_WM',
        roleDescription: 'Base Warehouse Manager'
    },
    Base_WE: {
        roleId: 3,
        roleName: 'Base_WE',
        roleDescription: 'Base Warehouse Employee'
    },
    Branch_WD: {
        roleId: 4,
        roleName: 'Branch_WD',
        roleDescription: 'Branch Warehouse Director'
    },
    Branch_WM: {
        roleId: 5,
        roleName: 'Branch_WM',
        roleDescription: 'Branch Warehouse Manager'
    },
    Branch_WE: {
        roleId: 6,
        roleName: 'Branch_WE',
        roleDescription: 'Branch Warehouse Employee'
    }
}
