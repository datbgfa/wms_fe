export const HEADER_MAPPING = {
    'product-management': 'Danh sách sản phẩm',
    home: 'Tổng quan',
    'account-management': 'Danh sách tài khoản',
    profile: 'Thông tin tài khoản',
    'category-management': 'Danh sách loại sản phẩm',
    'employee-management': 'Danh sách nhân viên',
    'employee-details': 'Thông nhân viên',
    'supplier-management': 'Danh sách nhà cung cấp',
    'supplier-details': 'Thông tin nhà cung cấp',
    'stock-quantity-management': 'Quản kí kho',
    'base-import-list': 'Danh sách đơn nhập',
    'create-base-import': 'Tạo đơn nhập hàng',
    'base-import-detail': 'Chi tiết đơn nhập',
    'update-base-import': 'Chỉnh sửa đơn nhập',
    'create-base-product-return': 'Tạo đơn trả hàng',
    'base-export-list': 'Danh sách đơn xuất',
    'base-export-detail': 'Chi tiết đơn xuất',
    'update-base-export': 'Chỉnh sửa đơn xuất',
    'create-branch-import-request': 'Tạo đơn yêu cầu nhập',
    'update-branch-import-request': 'Chỉnh sửa đơn yêu cầu nhập',
    'branch-import-request-detail': 'Chi tiết đơn yêu cầu nhập',
    'branch-import-list': 'Danh sách đơn nhập',
    'branch-import-detail': 'Chi tiết đơn nhập',
    'create-branch-product-return': 'Tạo đơn trả hàng',
    'branch-export-list': 'Danh sách đơn xuất',
    'branch-export-detail': 'Chi tiết đơn xuất',
    'create-branch-export': 'Tạo đơn xuất hàng',
    'create-product': 'Thêm sản phẩm mới',
    'stock-check-sheet-management': 'Danh sách kiểm hàng',
    'stock-check-sheet-details': 'Chi tiết kiểm hàng',
    'create-stock-check-sheet': 'Tạo đơn kiểm hàng',
    'view-warehouse-profile': 'Thông tin kho',
    'view-branch-warehouses': 'Danh sách kho nhánh',
    'base/product-return-management': 'Danh sách trả hàng',
    'branch-import-request': 'Đơn yêu cầu nhập hàng',
    'branch-product-management': 'Danh sách sản phẩm',
    'base-invoice': 'Danh sách phiếu',
    'branch-invoice': 'Danh sách phiếu'
}
