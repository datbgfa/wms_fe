import 'bootstrap/dist/css/bootstrap.min.css'
import '@/assets/styles/global.scss'
import 'primevue/resources/themes/lara-light-indigo/theme.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from '@/App.vue'
import router from '@/router'

import { registerGlobalComponents } from '@/utils/import.js'
import { usePrimeVue } from '@/primevue.globalcomp'

const app = createApp(App)

registerGlobalComponents(app)
usePrimeVue(app)
app.use(createPinia())
app.use(router)

app.mount('#app')

import 'bootstrap/dist/js/bootstrap.js'
